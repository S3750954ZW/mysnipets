#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;

enum Sex {
   MALE,
   FEMALE,
   DESEXED
};

class cat
{
private:
    std::string name;
    int age;
    Sex sex;
public:
    cat();
    cat(std::string,int,Sex);
    void desex();
    void meow();
    std::string getSexStr();
};

cat::cat()
{
    this -> name = "stray";
    this -> age = 0;
    this -> sex = MALE;
}

cat::cat(std::string name, int age, Sex sex)
{
    this -> name = name;
    this -> age = age;
    this -> sex = sex;
}

void cat::desex()
{
    this -> sex = DESEXED;
}

std::string cat::getSexStr()
{
    std::string outStr = "N/A";
    if (this -> sex == MALE)
    {
        outStr = "Male";
    }else if (this -> sex == FEMALE)
    {
        outStr = "Female";
    }else if (this -> sex == DESEXED)
    {
        outStr = "Desexed";
    }
    return outStr;
}

void cat::meow()
{
    cout<<"Cat "<<this->name<<" ("<<this->age<<"-year-old,"<<this->getSexStr()<<") just meowed!"<<endl;
}

int main(int argc, char** argv){
    char inputChar;
    char outStr[50] = {'\0'};
    int inputCount = 0;
    while ((int)inputChar!=4 && std::cin.get(inputChar))
    {
        if ((int)inputChar!=4)
        {
            outStr[inputCount] = inputChar;
            inputCount++;
        }else
        {
            outStr[inputCount]= '\0';
        }
    }
    cout<<outStr<<endl;
    cat hisCat;
    cat herCat = cat();
    cat myCat("Leah",10,MALE);
    cat strayPack[5]{cat(),cat(),cat(),cat(),cat()};
    cat* catPtr = new cat();
    cat* catArr = new cat[5]{cat(),cat(),cat(),cat(),cat()};
    cat** catPtrArr = new cat*[5]{new cat(),new cat(),new cat(),new cat(),new cat()};
    hisCat.meow();
    hisCat.desex();
    hisCat.meow();
    delete catPtr;
    catPtr = nullptr;
    for (int i = 0; i < 5; i++)
    {
        catArr[i].meow();
        delete catPtrArr[i];
        catPtrArr[i] = nullptr;
    }
    delete[] catArr;
    delete[] catPtrArr;
    catPtrArr = nullptr;
    
    return EXIT_SUCCESS;
}
