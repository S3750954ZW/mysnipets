void reverse_int(int* arrA, int* arrB, int length){
    if (arrA != nullptr && arrB != nullptr && length>0)
    {
        for (int i = 0; i < length; i++)
        {
            arrA[i] = arrB[length-i-1];
        }
    }
}