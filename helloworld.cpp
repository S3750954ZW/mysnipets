#include <iostream>
#include <string>

using std::cout;
using std::endl;

int foo();
int bar(char*);

int main(void)
{
   char* c = new char[5]{'1','2','3','4','5'};
   cout<<"old c is"<<endl;
   for (int i = 0; i < 5; i++)
   {
      cout<<c[i];
   }
   cout<<endl;
   foo();
   cout<<endl;
   cout<<"new c outside foo function is"<<endl;
   for (int i = 0; i < 5; i++)
   {
      cout<<c[i];
   }
   cout<<endl;
   bar(c);
   cout<<endl;
   cout<<"new c outside bar function is"<<endl;
   for (int i = 0; i < 48; i++)
   {
      cout<<c[i];
   }
   cout<<endl;
   delete[] c;
   return EXIT_SUCCESS;
}

// put an arbitrary number of characters into a dynamically allocated array on heap
int foo()
{
   char* c = new char[1]{'\0'};
   char* cTemp = new char[1]{'\0'};
   char cTarget[48] = "sjaoidjawoidhewwiwhjwuieaiwonskdpkdpaskpajsaoid";
   int len = 0;
   while (len<48)
   {
      delete[] cTemp;
      c[len] = cTarget[len];
      cTemp = new char[len+1];
      for (int i = 0; i < len+1; i++)
      {
         cTemp[i] = c[i];
      }
      delete[] c;
      len++;
      c = new char[len+1];
      for (int i = 0; i < len; i++)
      {
         c[i] = cTemp[i];
      }
            
   }
   delete[] cTemp;
   cout<<"new c in foo function is"<<endl;
   for (int i = 0; i < 48; i++)
   {
      cout<<c[i];
   }
   delete[] c;
   return EXIT_SUCCESS;
   
}

// clean up c defined in main, then put an arbitrary number of characters into the c array on heap
int bar(char* c)
{
   delete[] c;
   c = new char[1]{'\0'};
   char* cTemp = new char[1]{'\0'};
   char cTarget[48] = "sjaoidjawoidhewwiwhjwuieaiwonskdpkdpaskpajsaoid";
   int len = 0;
   while (len<48)
   {
      delete[] cTemp;
      c[len] = cTarget[len];
      cTemp = new char[len+1]{'\0'};
      for (int i = 0; i < len+1; i++)
      {
         cTemp[i] = c[i];
      }
      delete[] c;
      len++;
      c = new char[len+1];
      for (int i = 0; i < len; i++)
      {
         c[i] = cTemp[i];
      }
            
   }
   delete[] cTemp;
   cout<<"new c in bar function is"<<endl;
   for (int i = 0; i < 48; i++)
   {
      cout<<c[i];
   }
   return EXIT_SUCCESS;
   
}