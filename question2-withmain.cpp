#include <iostream>

using std::cout;
using std::endl;
using std::cin;

void reverse_int(int* arrA, int* arrB, int length);

int main(void)
{
    int length = 5;
    int A[5] = {1,2,3,4,5};
    int B[5] = {3,5,2,4,6};
    cout<<"before reverse:"<<endl;
    for (int i = 0; i < length; i++)
    {
        cout<<"A["<<i<<"] is "<<A[i]<<" and B["<<i<<"] is "<<B[i]<<endl;
    }
    
    reverse_int(A,B,length);
    cout<<"after reverse:"<<endl;
    for (int i = 0; i < length; i++)
    {
        cout<<"A["<<i<<"] is "<<A[i]<<" and B["<<i<<"] is "<<B[i]<<endl;
    }
    return EXIT_SUCCESS;
}

void reverse_int(int* arrA, int* arrB, int length){
    if (arrA != nullptr && arrB != nullptr && length>0)
    {
        for (int i = 0; i < length; i++)
        {
            arrA[i] = arrB[length-i-1];
        }
    }
}
